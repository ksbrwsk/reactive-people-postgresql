package de.ksbrwsk.people;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.springframework.data.annotation.Id;

public record Person(@Id Long id, @NotNull @Size(max = 10, min = 1) String name) {
}
