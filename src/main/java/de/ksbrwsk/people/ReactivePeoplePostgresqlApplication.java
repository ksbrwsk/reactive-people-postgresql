package de.ksbrwsk.people;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactivePeoplePostgresqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactivePeoplePostgresqlApplication.class, args);
	}

}
