CREATE TABLE if not exists person
(
    id BIGSERIAL,
    name varchar(255) NULL,
    vorname varchar(255) NULL,
    CONSTRAINT partner_pkey PRIMARY KEY(id)
);
