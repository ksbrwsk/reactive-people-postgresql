package de.ksbrwsk.people;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Optional;

import static de.ksbrwsk.people.PersonHandler.API;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FullIntegrationTest extends AbstractIntegrationTest {
    @LocalServerPort
    int port;

    @Autowired
    PersonRepository personRepository;

    static WebTestClient webTestClient;

    @BeforeEach
    void setUp() {
        Mono<Long> longMono = this.personRepository
                .deleteAll()
                .then(this.personRepository.save(new Person(null, "Name1")))
                .then(this.personRepository.save(new Person(null, "Name2")))
                .then(this.personRepository.count());
        StepVerifier
                .create(longMono)
                .expectNext(2L)
                .verifyComplete();
    }

    private WebTestClient webTestClient() {
        if (webTestClient == null) {
            webTestClient = WebTestClient
                    .bindToServer()
                    .baseUrl(String.format("http://localhost:%d", this.port))
                    .build();
        }
        return webTestClient;
    }

    @Test
    void handleNotFound() {
        this.webTestClient()
                .get()
                .uri("/api/peple")
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @Test
    void handleFindAll() {
        this.webTestClient()
                .get()
                .uri(API)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$[0].name").isEqualTo("Name1")
                .jsonPath("$[1].name").isEqualTo("Name2");
    }

    @Test
    void handleFindByIdValid() {
        Person expected = this.personRepository
                .findAll()
                .take(1L)
                .blockFirst();
        this.webTestClient()
                .get()
                .uri(API + "/" + expected.id())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.id").isEqualTo(expected.id())
                .jsonPath("$.name").isEqualTo(expected.name());
    }

    @Test
    void handleFindByIdNotFound() {
        this.webTestClient()
                .get()
                .uri(API + "/4711")
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @Test
    void handleDeleteById() {
        Person expected = this.personRepository
                .findAll()
                .take(1L)
                .blockFirst();
        this.webTestClient()
                .delete()
                .uri(API + "/" + expected.id())
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$").isEqualTo("successfully deleted!");
    }

    @Test
    void handleDeleteByIdNotFound() {
        this.webTestClient()
                .delete()
                .uri(API + "/4711")
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @Test
    void handleUpdate() {
        Person expected = this.personRepository
                .findAll()
                .take(1L)
                .blockFirst();
        this.webTestClient()
                .put()
                .uri(API + "/" + expected.id())
                .bodyValue(new Person(expected.id(), "Update"))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.id").isEqualTo(expected.id())
                .jsonPath("$.name").isEqualTo("Update");
    }

    @Test
    void handleUpdateBadRequest() {
        Person expected = this.personRepository
                .findAll()
                .take(1L)
                .blockFirst();
        this.webTestClient()
                .put()
                .uri(API + "/" + expected.id())
                .bodyValue(Optional.empty())
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    void handleUpdateNotFound() {
        this.webTestClient()
                .put()
                .uri(API + "/4711")
                .bodyValue(new Person(4711L, "Update"))
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @ParameterizedTest
    @ValueSource(strings = {"N", "Name", "0123456789"})
    void handleCreateValid(String name) {
        this.webTestClient()
                .post()
                .uri(API)
                .bodyValue(new Person(null, name))
                .exchange()
                .expectStatus()
                .isCreated()
                .expectHeader()
                .exists("Location")
                .expectBody()
                .jsonPath("$.id").exists()
                .jsonPath("$.name").isEqualTo(name);
    }

    @Test
    void handleCreateBadRequest() {
        this.webTestClient()
                .post()
                .uri(API)
                .bodyValue(Optional.empty())
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @ParameterizedTest
    @ValueSource(strings = {"00123456789", "012345678900054545"})
    void handleCreateInvalid(String name) {
        this.webTestClient()
                .post()
                .uri(API)
                .bodyValue(new Person(null, name))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }
}
