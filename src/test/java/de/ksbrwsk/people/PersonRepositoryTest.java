package de.ksbrwsk.people;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;

@DataR2dbcTest
class PersonRepositoryTest extends AbstractIntegrationTest {
    @Autowired
    PersonRepository personRepository;

    @Test
    void persist() {
        Mono<Long> longMono = this.personRepository
                .deleteAll()
                .then(this.personRepository.save(new Person(null, "Name1")))
                .then(this.personRepository.save(new Person(null, "Name2")))
                .then(this.personRepository.count());
        StepVerifier
                .create(longMono)
                .expectNext(2L)
                .verifyComplete();
    }

    @Test
    void findById() {
        Mono<Person> personMono = this.personRepository
                .deleteAll()
                .then(this.personRepository.save(new Person(null, "Name")))
                .flatMap(person -> this.personRepository.findById(person.id()));
        StepVerifier
                .create(personMono)
                .expectNextMatches(person -> person.id() != null &&
                        person.name().equalsIgnoreCase("name"))
                .verifyComplete();
    }

    @Test
    void delete() {
        Mono<Long> longMono = this.personRepository
                .deleteAll()
                .then(this.personRepository.save(new Person(null, "Name")))
                .flatMap(this.personRepository::delete)
                .then(this.personRepository.count());
        StepVerifier
                .create(longMono)
                .expectNext(0L)
                .verifyComplete();
    }

    @Test
    void testdata() {
        List<Person> people = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            people.add(new Person(null, "Person@" + i));
        }
        Flux<Person> personFlux = this.personRepository
                .deleteAll()
                .thenMany(this.personRepository.saveAll(people));
        StepVerifier
                .create(personFlux)
                .expectNextCount(100L)
                .verifyComplete();
    }
}