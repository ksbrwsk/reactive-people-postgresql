package de.ksbrwsk.people;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;

import static de.ksbrwsk.people.PersonHandler.API;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebFluxTest
@Import({PersonHandler.class, PersonRouter.class})
class PersonRouterTest {
    @Autowired
    WebTestClient webTestClient;

    @MockBean
    PersonRepository personRepository;

    @Test
    void handleNotFound() {
        this.webTestClient
                .get()
                .uri("/api/peple")
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @Test
    void handleFindAll() {
        when(this.personRepository.findAll())
                .thenReturn(Flux.just(
                        new Person(1L, "Name1"),
                        new Person(2L, "Name2")
                ));
        this.webTestClient
                .get()
                .uri(API)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$[0].name").isEqualTo("Name1")
                .jsonPath("$[1].name").isEqualTo("Name2");
    }

    @Test
    void handleFindById() {
        when(this.personRepository.findById(1L))
                .thenReturn(Mono.just(new Person(1L, "Name")));
        this.webTestClient
                .get()
                .uri(API + "/1")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Person.class)
                .isEqualTo(new Person(1L, "Name"));
    }

    @Test
    void handleFindByIdNotFound() {
        when(this.personRepository.findById(1L))
                .thenReturn(Mono.empty());
        this.webTestClient
                .get()
                .uri(API + "/1")
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @Test
    void handleDeleteById() {
        when(this.personRepository.findById(1L))
                .thenReturn(Mono.just(new Person(1L, "Name")));
        when(this.personRepository.delete(any(Person.class)))
                .thenReturn(Mono.empty());
        this.webTestClient
                .delete()
                .uri(API + "/1")
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$").isEqualTo("successfully deleted!");
    }

    @Test
    void handleDeleteByIdNotFound() {
        when(this.personRepository.findById(1L))
                .thenReturn(Mono.empty());
        this.webTestClient
                .delete()
                .uri(API + "/1")
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @Test
    void handleUpdate() {
        when(this.personRepository.findById(1L))
                .thenReturn(Mono.just(new Person(1L, "Old")));
        when(this.personRepository.save(new Person(1L, "Update")))
                .thenReturn(Mono.just(new Person(1L, "Update")));
        this.webTestClient
                .put()
                .uri(API + "/1")
                .bodyValue(new Person(1L, "Update"))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Person.class)
                .isEqualTo(new Person(1L, "Update"));
    }

    @Test
    void handleUpdateNotFound() {
        when(this.personRepository.findById(1L))
                .thenReturn(Mono.empty());
        this.webTestClient
                .put()
                .uri(API + "/1")
                .bodyValue(new Person(1L, "Update"))
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @Test
    void handleUpdateBadRequest() {
        when(this.personRepository.findById(1L))
                .thenReturn(Mono.just(new Person(1L, "Old")));
        this.webTestClient
                .put()
                .uri(API + "/1")
                .bodyValue(Optional.empty())
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    void handleCreate() {
        when(this.personRepository.save(new Person(null, "Name")))
                .thenReturn(Mono.just(new Person(1L, "Name")));
        this.webTestClient
                .post()
                .uri(API)
                .bodyValue(new Person(null, "Name"))
                .exchange()
                .expectStatus()
                .isCreated()
                .expectHeader()
                .location(API + "/1")
                .expectBody(Person.class)
                .isEqualTo(new Person(1L, "Name"));
    }

    @Test
    void handleCreateBadRequest() {
        when(this.personRepository.save(new Person(null, "Name")))
                .thenReturn(Mono.just(new Person(1L, "Name")));
        this.webTestClient
                .post()
                .uri(API)
                .bodyValue(Optional.empty())
                .exchange()
                .expectStatus()
                .isBadRequest();
    }
}