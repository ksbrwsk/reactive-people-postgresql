package de.ksbrwsk.people;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PersonTest {

    @Test
    void create() {
        Person person = new Person(1L, "Name");
        assertEquals(1L, person.id());
        assertEquals("Name", person.name());
    }
}