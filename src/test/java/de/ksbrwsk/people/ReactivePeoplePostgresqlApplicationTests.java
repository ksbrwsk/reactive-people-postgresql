package de.ksbrwsk.people;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ReactivePeoplePostgresqlApplicationTests {

	@Test
	void contextLoads() {
		ReactivePeoplePostgresqlApplication.main(new String[]{});
	}

}
